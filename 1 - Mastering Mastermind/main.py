import random

colorList = ['R', 'G', 'B', 'W', 'Y', 'O']


# Creates a random answer
def CreateAnswer():
    answer = []
    for x in range(4):
        answer.append(colorList[random.randrange(0, 6)])
    return answer


# Asks the player for a code combination, this is used when the player is playing as codebreaker or codemaker
def playerInput():
    guess = []
    print(f'Kies uit de volgende opties {colorList}')
    for x in range(4):
        guessRaw = input(f'Kies je {x + 1}e kleur: ').capitalize()
        while True:
            if guessRaw not in colorList:
                guessRaw = input('Ongeldige invoer probeer opnieuw\n' f'Kies je {x + 1}e kleur: ').capitalize()
            else:
                guess.append(guessRaw)
                break
    return guess


# Creates a list with all the possible color combinations
def createPossibleCodes(colors):
    possiblecodes = []
    for i in range(0, len(colors)):
        for a in range(i, len(colors)):
            for b in range(i, len(colors)):
                for c in range(i, len(colors)):
                    out = [colors[i], colors[a], colors[b], colors[c]]
                    possiblecodes.append(out)
    return possiblecodes


# Evaluates the guess and returns a score
def evaluate(guess, secret):
    score = [0, 0]
    used = []

    for position in range(len(guess)):
        if guess[position] == secret[position]:
            score[0] += 1
            used.append(position)

    secretCopy = list(secret[::])

    for position in used:
        secretCopy.remove(secret[position])

    for i in range(len(guess)):
        if i not in used:
            if guess[i] in secretCopy:
                score[1] += 1
                secretCopy.remove(guess[i])
    return score


# Reduces the list created by the def createPossibleCodes function using the score obtained by the def evaluate function
def reduce(possibleCodes, code, score):
    result = []
    for possibleCode in possibleCodes:
        if score == evaluate(code, possibleCode):
            result.append(possibleCode)

    return result


# Initializes a game where the player is the codebreaker and the computer is the codemaker
def playerCodebreaker():
    print(f'\nHet spel wordt opgestart met de speler als codebreaker\n')
    guess = playerInput()
    secret = CreateAnswer()
    attemptCount = 0
    while True:

        attemptCount += 1

        if guess == secret:
            print(f'Je bent echt een mastermind! Je had {attemptCount} pogingen nodig')
            return

        else:
            print(
                f'Je gok: {guess} heeft: {evaluate(guess, secret)[0]} zwarte pin(nen) en {evaluate(guess, secret)[1]} '
                f'witte pin(nen)\n\nPoging {attemptCount + 1}:')
            guess = playerInput()

        if attemptCount > 8:
            print(
                f'Helaas pindakaas, Je hebt geen pogingen meer. Het antwoord was {secret}.'
                f' run de pogramme opnieuw als je nog een poging wilt wagen.')
            return


# Initializes a game where the computer is the codebreaker and the player is the codemaker
def computerCodebreaker():
    print(f'\nHet spel wordt opgestart met de computer als codebreaker\n')
    count = 0
    possibleCodesLst = createPossibleCodes(colorList)
    secretAnswer = playerInput()
    while count < 8:
        if count == 0:
            nextGuess = possibleCodesLst[0]

        elif count == 1:
            nextGuess = possibleCodesLst[(random.randint(0, (len(possibleCodesLst) - 1)))]

        else:
            possibleCodesLst = reduce(possibleCodesLst, nextGuess, evaluate(nextGuess, secretAnswer))
            if len(possibleCodesLst) == 0:
                possibleCodesLst = createPossibleCodes(colorList)
            nextGuess = possibleCodesLst[(random.randint(0, (len(possibleCodesLst) - 1)))]

        print(f'De computer gokt: {nextGuess}')
        print(
            f'Zwarte Pins:{evaluate(nextGuess, secretAnswer)[0]}, Witte Pins:{evaluate(nextGuess, secretAnswer)[1]}\n')

        if evaluate(nextGuess, secretAnswer)[0] == 4:
            print(f'De computer heeft het geraden in: {count} pogingen')
            return

        count += 1

    if count == 8:
        print(f'De computer heeft je code niet kunnen kraken.')
        return


# Initializes the program/game
def optionScreen():
    print(f'Welkom bij Mastermind')

    while True:
        print(
            'Kies uit de volgende opties:\n'
            '1. De computer als codemaker en u als codebreaker\n'
            '2. De computer als codebreaker en u als codemaker\n'
            '3. Uitleg')
        playerChoice = str((input(f'Keuze: ')))
        if playerChoice == '1':
            playerCodebreaker()
            break

        elif playerChoice == '2':
            computerCodebreaker()
            break

        elif playerChoice == '3':
            print(f'\nDe codebreker probeert het patroon te raden, zowel in volgorde als kleur, binnen acht beurten.\n'
                  f'Elke gok wordt gedaan door een rij van codepinnen op het decodeerbord te plaatsen.\n'
                  f'Eenmaal geplaatst, geeft de codebreker feedback. \nEen gekleurde of zwarte pin wordt geplaatst'
                  f'voor elk codepinnetje van de gok dat correct is in zowel kleur als positie. \nEen witte pin geeft '
                  f'aan dat '
                  f'er een pin met de juiste kleur op de verkeerde positie is geplaatst.\n')


if __name__ == '__main__':
    optionScreen()
