import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["shopping-minds"]
mycol = mydb["products"]

# Wat is de naam en prijs van het eerste product in de database?
a = mycol.find_one({}, {"_id": 0, "name": 1, "price": 1})
print(a)

# Geef de naam van het eerste product waarvan de naam begint met een 'R'?
myquery1 = {"name": {"$regex": "^R"}}
b = mycol.find_one(myquery1, {"_id": 0, "name": 1, "price": 1})
print(b)

# Wat is de gemiddelde prijs van de producten in de database?
prices = []
for c in mycol.find({}, {"_id": 0, "price.selling_price": 1}):
    if c.get('price', {}).get('selling_price'):
        prices.append(c.get('price', {}).get('selling_price'))
print(f'De gemiddelde prijs is:  {round(sum(prices) / len(prices), 2)}')
